// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyD6o17uSzz79oaU42bj9nVrMfJt0JDba24',
    authDomain: 'controle-te.firebaseapp.com',
    projectId: 'controle-te',
    storageBucket: 'controle-te.appspot.com',
    messagingSenderId: '518128570995',
    appId: '1:518128570995:web:c438812fc2daf8a379d006',
    measurementId: 'G-FKD742F5F7'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
